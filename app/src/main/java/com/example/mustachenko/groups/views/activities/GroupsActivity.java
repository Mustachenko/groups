package com.example.mustachenko.groups.views.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.pm.PackageManager;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.example.mustachenko.groups.App;
import com.example.mustachenko.groups.R;
import com.example.mustachenko.groups.factories.DaggerViewModelFactory;
import com.example.mustachenko.groups.viewModels.GroupsViewModel;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;

import javax.inject.Inject;

public class GroupsActivity extends AppCompatActivity {
    private MapView mapView;
    private FloatingSearchView mapSearchView;
    private GroupsViewModel viewModel;

    @Inject
    public LinearLayoutManager linearLayoutManager;

    @Inject
    DaggerViewModelFactory viewModelFactory;

    private BottomSheetBehavior bottomSheetBehavior;

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION}, 106);
        mapInit();
    }

    private void mapInit() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestForSpecificPermission();
        } else {
            mapView.getMapAsync(onMapReady);
        }
    }

    @SuppressLint("MissingPermission")
    private OnMapReadyCallback onMapReady = googleMap -> {
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.setOnCameraMoveListener(() ->
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN));
    };



    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);

        ((App) getApplication()).component().injectGroupActivity(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(GroupsViewModel.class);

        DrawerLayout root = findViewById(R.id.root);
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onSaveInstanceState(savedInstanceState);
        mapView.getMapAsync(onMapReady);
        mapSearchView = findViewById(R.id.search_view);
        mapSearchView.bringToFront();
        mapSearchView.attachNavigationDrawerToMenuButton(root);
        mapInit();
        bottomSheetInit();
       }

    private void bottomSheetInit() {
        float searchHeight = getResources().getDimensionPixelSize(R.dimen.mapSearchViewAreaHeight);
        LinearLayout fabLayout = findViewById(R.id.fabLayout);
        NestedScrollView bottomSheet = findViewById(R.id.bottomSheet);
        TextView amount = findViewById(R.id.amount);
        View slider = findViewById(R.id.slider);
        RecyclerView recyclerView = findViewById(R.id.groups);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setPeekHeight(getWindow().getWindowManager().getDefaultDisplay().getHeight() >> 1);
        TransitionDrawable background = (TransitionDrawable) getResources().getDrawable(R.drawable.bottom_dialog_background_transition);
        bottomSheet.setBackground(background);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                if (i == BottomSheetBehavior.STATE_EXPANDED) {
                    slider.animate().scaleX(0.3f).alpha(0f).scaleY(0.6f).setStartDelay(210);
                }
                if (i == BottomSheetBehavior.STATE_COLLAPSED || i == BottomSheetBehavior.STATE_HIDDEN) {
                    mapSearchView.animate().translationY(0).scaleX(1f).setDuration(100).setStartDelay(0).start();
                    slider.animate().scaleX(1).scaleY(1).alpha(1f).setStartDelay(210);
                }
                if (i == BottomSheetBehavior.STATE_DRAGGING) {
                    mapSearchView.animate().translationY(-searchHeight).scaleX(0.9f).setDuration(100).setStartDelay(0).start();
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {
                if (v == 1.0f) {
                    background.startTransition(200);
                } else { background.resetTransition();}

                if (v > 0.1f) {
                   fabLayout.animate().translationX(-150).scaleX(0.9f).scaleY(0.9f).setDuration(100);
                } else {
                    fabLayout.animate().translationX(0).scaleX(1f).scaleY(1f).setDuration(100);
                }
            }
        });
        recyclerView.setAdapter(viewModel.getGroupsRecyclerViewAdapter());

        viewModel.getGroupsRecyclerViewAdapter()
                .setItemCheckedChangeListener(() -> amount.setText("Amount: " + String.format("%.2f", viewModel.amount()) + " UAH"));
        recyclerView.setLayoutManager(linearLayoutManager);
        viewModel.setAdapterBinding(() -> viewModel.getGroupsRecyclerViewAdapter().populate(viewModel.getGroups()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
        viewModel.connectToApi();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }
}
