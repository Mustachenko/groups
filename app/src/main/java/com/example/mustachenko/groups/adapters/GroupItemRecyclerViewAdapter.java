package com.example.mustachenko.groups.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mustachenko.groups.POJOs.GroupItem;
import com.example.mustachenko.groups.R;

import java.util.List;

import cn.refactor.library.SmoothCheckBox;
import nl.dionsegijn.steppertouch.StepperTouch;

public class GroupItemRecyclerViewAdapter extends RecyclerView.Adapter<GroupItemRecyclerViewAdapter.ViewHolder> {
    private List<GroupItem> items;
    private ItemCheckedChangeListener itemCheckedChangeListener;

    void setItemCheckedChangeListener(ItemCheckedChangeListener itemCheckedChangeListener) {
        this.itemCheckedChangeListener = itemCheckedChangeListener;
    }

    public void insertFields(List<GroupItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        RelativeLayout root = viewHolder.root;
        viewHolder.checkBox = root.findViewById(R.id.checkbox);
        viewHolder.image = root.findViewById(R.id.item_picture);
        viewHolder.name = root.findViewById(R.id.item_name);
        viewHolder.price = root.findViewById(R.id.item_price);
        viewHolder.separator = root.findViewById(R.id.separator);
        viewHolder.stepper = root.findViewById(R.id.stepper);
        viewHolder.name.setText(items.get(i).getName());
        double price = items.get(i).getPrice();
        if (price != 0.00f) {
            viewHolder.price.setText(String.format("%.2f", items.get(i).getPrice()) + " UAH");
        }

        if (i == getItemCount() - 1) {
            viewHolder.separator.setVisibility(View.GONE);
        }

        if (viewHolder.name.getText().toString().isEmpty()) {
            viewHolder.name.setBackgroundResource(R.drawable.rounded_input_field);
            AlphaAnimation pulse = new AlphaAnimation(1, 0.4f);
            pulse.setDuration(500);
            pulse.setRepeatCount(Animation.INFINITE);
            pulse.setRepeatMode(Animation.REVERSE);
            viewHolder.name.startAnimation(pulse);
            viewHolder.price.startAnimation(pulse);
            viewHolder.image.startAnimation(pulse);
        }

        viewHolder.stepper.stepper.setMin(1);
        viewHolder.stepper.stepper.setValue(1);
        viewHolder.stepper.enableSideTap(true);
        if (items.get(i).isChecked()) {
            viewHolder.checkBox.setChecked(true);
        } else {
            viewHolder.checkBox.setChecked(false);
        }
        viewHolder.checkBox.setOnCheckedChangeListener((v, b) -> {
            items.get(viewHolder.getAdapterPosition()).setChecked(b);
            itemCheckedChangeListener.listener();
        });
        viewHolder.stepper.stepper.addStepCallback((count, b) -> {
            items.get(viewHolder.getAdapterPosition()).setCount(count);
            itemCheckedChangeListener.listener();
        });
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RelativeLayout root = (RelativeLayout) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.group_item_layout, viewGroup, false);
        return new ViewHolder(root);
    }

    public interface ItemCheckedChangeListener {
        void listener();
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout root;
        ImageView image;
        TextView name;
        TextView price;
        StepperTouch stepper;
        SmoothCheckBox checkBox;
        View separator;

        ViewHolder(@NonNull RelativeLayout itemView) {
            super(itemView);
            root = itemView;
        }
    }
}
