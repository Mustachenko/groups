package com.example.mustachenko.groups.dagger.modules;

import com.example.mustachenko.groups.App;
import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {


    @Provides @Singleton
    HttpLoggingInterceptor httpLogProvider() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Provides @Singleton
    OkHttpClient OkHttpclientProvider(Cache cache, HttpLoggingInterceptor httpLog) {
        return new OkHttpClient.Builder()
                //.cache(cache)
                .addInterceptor(httpLog)
                .build();
    }

    @Singleton
    @Provides
    Gson GsonProvider() {
        return new Gson();
    }

    @Provides
    Retrofit RetrofitClientProvider(OkHttpClient client, Gson gson) {
        String URL = "http://test53.phpist.com.ua/";
          return new Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create(gson)).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).client(client).build();
    }

    @Singleton
    @Provides
    Cache NetworkCacheProvider(App application) {
        long cashSize = (long) 5 * 1024 * 1024;
        return new Cache(application.getCacheDir(), cashSize);
    }

}
