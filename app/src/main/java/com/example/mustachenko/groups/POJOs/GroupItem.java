package com.example.mustachenko.groups.POJOs;

import com.google.gson.annotations.SerializedName;

public class GroupItem {
    private String imageUrl;

    @SerializedName("price")
    private double price;
    @SerializedName("name")
    private String name;
    private int count = 1;
    private boolean isChecked;

    public GroupItem() {
    }

    public GroupItem(String imageUrl, int price, String name, int count, boolean isChecked) {
        this.imageUrl = imageUrl;
        this.price = price;
        this.name = name;
        this.count = count;
        this.isChecked = isChecked;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
