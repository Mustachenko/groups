package com.example.mustachenko.groups.POJOs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Group {
    @SerializedName("group")
    private String groupName;
    private String imageUrl;
    @SerializedName("products")
    private List<GroupItem> items;

    public Group(String groupName, String imageUrl, List<GroupItem> items) {
        this.groupName = groupName;
        this.imageUrl = imageUrl;
        this.items = items;
    }

    public Group() {
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<GroupItem> getItems() {
        return items;
    }

    public void setItems(List<GroupItem> items) {
        this.items = items;
    }
}
