package com.example.mustachenko.groups.viewModels;

import android.arch.lifecycle.AndroidViewModel;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.mustachenko.groups.API.GroupsAPI;
import com.example.mustachenko.groups.App;
import com.example.mustachenko.groups.POJOs.Group;
import com.example.mustachenko.groups.POJOs.GroupItem;
import com.example.mustachenko.groups.adapters.GroupsRecyclerViewAdapter;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class GroupsViewModel extends AndroidViewModel {
    @Inject
    public Retrofit retrofit;
    @Inject
    public GroupsRecyclerViewAdapter groupsRecyclerViewAdapter;
    private ArrayList<Group> groups;
    private GroupsAPI.ApiGroupInterface apiGroupInterface;
    private AdapterBinding adapterBinding;

    @Inject
    public GroupsViewModel(@NonNull App application) {
        super(application);
        groups = new ArrayList<>();
    }

    public AdapterBinding getAdapertBinding() {
        return adapterBinding;
    }

    public void setAdapterBinding(AdapterBinding adapterBinding) {
        this.adapterBinding = adapterBinding;
    }


    public double amount() {
        double amount = 0;
        for (Group group : groups) {
            for (GroupItem item : group.getItems()) {
                if (item.isChecked()) {
                    amount += (item.getPrice() * item.getCount());
                }
            }
        }
        return amount;
    }

    public GroupsRecyclerViewAdapter getGroupsRecyclerViewAdapter() {
        return groupsRecyclerViewAdapter;
    }

    public ArrayList<Group> getGroups() {
        return groups;
    }

    public void connectToApi() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplication().getSystemService(CONNECTIVITY_SERVICE);
        if (Objects.requireNonNull(connectivityManager).isDefaultNetworkActive()) {
            getGroupsFromApi();
        } else connectivityManager.addDefaultNetworkActiveListener(this::getGroupsFromApi);
    }

    private void getGroupsFromApi() {

        SingleObserver<Group[]> subscriber = new SingleObserver<Group[]>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onSuccess(Group[] groups) {
                GroupsViewModel.this.groups = new ArrayList<>(Arrays.asList(groups));
                adapterBinding.bind();
            }

            @Override
            public void onError(Throwable e) {
                Log.e("API", "onError: ", e);
            }

        };

        apiGroupInterface = retrofit.create(GroupsAPI.ApiGroupInterface.class);
        Single<Group[]> groupObservable = apiGroupInterface.getGroups();
        groupObservable.doOnError(throwable -> Log.i("On Error", "accept: " + throwable.getMessage()))
                .retryWhen(throwableFlowable -> throwableFlowable.zipWith(Flowable.range(1, 3),
                        (BiFunction<Throwable, Integer, Object>) (throwable, integer) -> {
                            Exception exception = new Exception(throwable);
                            if (exception instanceof HttpException) {
                                int code = ((HttpException) exception).code();
                                if (code != 505 && code != 404 && code < 500) {
                                    return integer;
                                } else throw new Exception();
                            }
                            return 1;
                        }).flatMap(retryCount -> Flowable.timer((long) Math.pow(2, (Integer) retryCount), TimeUnit.SECONDS)))
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(subscriber);
    }

    public interface AdapterBinding {
        void bind();
    }
}
