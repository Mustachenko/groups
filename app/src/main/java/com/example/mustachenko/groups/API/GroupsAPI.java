package com.example.mustachenko.groups.API;


import com.example.mustachenko.groups.POJOs.Group;

import io.reactivex.Single;
import retrofit2.http.GET;

public class GroupsAPI {

//    public static Retrofit getClient(Context context, String url) {
//        if (retrofit == null) {
//            long cashSize = (long) 5 * 1024 * 1024;
//            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//// set your desired log level
//            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//            //Cache cache = new Cache(context.getCacheDir(), cashSize);
//            OkHttpClient client = new OkHttpClient.Builder()
//                    //.cache(cache)
//                    .addInterceptor(logging)
//                    .build();
//            Gson gson = new GsonBuilder().setLenient()
//                    .create();
//            retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create(gson)).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).client(client).build();
//        }
//        return retrofit;
//    }

    public interface ApiGroupInterface {
        @GET("test.json")
        Single<Group[]> getGroups();
    }
}
