package com.example.mustachenko.groups.dagger.modules;

import android.arch.lifecycle.ViewModel;
import com.example.mustachenko.groups.viewModels.GroupsViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class MapActivityModule {
    @Binds
    @IntoMap
    @ViewModelKey(GroupsViewModel.class)
    abstract ViewModel bindUserViewModel(GroupsViewModel viewModel);
}
