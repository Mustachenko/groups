package com.example.mustachenko.groups.factories;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import java.util.Map;

import javax.inject.Inject;

import javax.inject.Provider;
import javax.inject.Singleton;

@Singleton
public class DaggerViewModelFactory implements ViewModelProvider.Factory {
    private final Map<Class<? extends ViewModel>, Provider<ViewModel>> creators;

    @Inject
    public DaggerViewModelFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> creators) {
        this.creators = creators;
    }

    private Provider<? extends ViewModel> setChecker(  Provider<? extends ViewModel> creator, Class<? extends ViewModel> modelClass ) {
        Provider<? extends ViewModel> resultCreator = null;
        if (creator == null) {
            for (Map.Entry<Class<? extends ViewModel>, Provider<ViewModel>> entry : creators.entrySet()) {
                if (modelClass.isAssignableFrom(entry.getKey())) {
                    resultCreator =  entry.getValue();
                }
            }
        }
        return resultCreator;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        Provider<? extends ViewModel> creator = creators.get(modelClass);
        Provider<? extends ViewModel> creatorValue = setChecker(creator, modelClass);
        if (creatorValue != null) {
            creator = creatorValue;
        }
        if (creator == null) {
            throw new IllegalArgumentException("unknown model class " + modelClass);
        }
        try {
            return (T) creator.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}