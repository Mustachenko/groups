package com.example.mustachenko.groups.dagger.modules;

import android.support.v7.widget.LinearLayoutManager;

import com.example.mustachenko.groups.App;
import com.example.mustachenko.groups.POJOs.Group;
import com.example.mustachenko.groups.POJOs.GroupItem;
import com.example.mustachenko.groups.adapters.GroupItemRecyclerViewAdapter;
import com.example.mustachenko.groups.adapters.GroupsRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import dagger.Module;
import dagger.Provides;

@Module
public class ListComponentsModule {

    @Provides
    GroupsRecyclerViewAdapter groupsRecyclerViewAdapterProvider(GroupItemRecyclerViewAdapter groupItemRecyclerViewAdapter, App app) {
        ArrayList<Group> groups = new ArrayList<>();
        return new GroupsRecyclerViewAdapter(groups, true, groupItemRecyclerViewAdapter, app);
    }

    @Provides
    List<Group> placeHolderFieldsProvider() {
        Group group = new Group();
        group.setGroupName("Group");
        List<GroupItem> items = new ArrayList<>();
        items.add(new GroupItem());
        items.add(new GroupItem());
        items.add(new GroupItem());
        group.setItems(items);
        List<Group> groups = new ArrayList<>();
        groups.add(group);
        return groups;
    }

    @Provides
    GroupItemRecyclerViewAdapter groupItemRecyclerViewAdapterProvider() {
        return new GroupItemRecyclerViewAdapter();
    }

    @Provides
    LinearLayoutManager linearLayoutManagerProvider(App application) {
        return new LinearLayoutManager(application);
    }
}
