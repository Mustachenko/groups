package com.example.mustachenko.groups;

import com.example.mustachenko.groups.dagger.components.ApplicationComponent;
import com.example.mustachenko.groups.dagger.components.DaggerApplicationComponent;


public class App extends android.app.Application {
    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerApplicationComponent
                .builder().
                        withApplication(this)
                .build();
        component.inject(this);

    }

    public ApplicationComponent component() {
        return component;
    }
}
