package com.example.mustachenko.groups.dagger.components;

import android.support.v7.widget.LinearLayoutManager;

import com.example.mustachenko.groups.App;
import com.example.mustachenko.groups.POJOs.Group;
import com.example.mustachenko.groups.dagger.modules.ListComponentsModule;
import com.example.mustachenko.groups.dagger.modules.MapActivityModule;
import com.example.mustachenko.groups.dagger.modules.NetworkModule;
import com.example.mustachenko.groups.views.activities.GroupsActivity;

import java.util.List;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
@Singleton
@Component(modules = {MapActivityModule.class, ListComponentsModule.class, NetworkModule.class})
public interface ApplicationComponent {

    LinearLayoutManager getLinearLayoutManager();

    List<Group> getGroupsPlaceHolder();

    void inject(App application);

    void injectGroupActivity(GroupsActivity activity);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder withApplication(App application);

 //       @BindsInstance
//        Builder networkModule(NetworkModule module);
//        @BindsInstance
//        Builder mapActivity(MapActivityModule module);
//        @BindsInstance
//        Builder listComponents(ListComponentsModule module);

        ApplicationComponent build();
    }

}
