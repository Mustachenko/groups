package com.example.mustachenko.groups.adapters;

import android.support.annotation.NonNull;
import android.support.design.card.MaterialCardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mustachenko.groups.App;
import com.example.mustachenko.groups.POJOs.Group;
import com.example.mustachenko.groups.R;

import java.util.List;

import javax.inject.Inject;

public class GroupsRecyclerViewAdapter extends RecyclerView.Adapter<GroupsRecyclerViewAdapter.ViewHolder> {
    private List<Group> groups;
    private GroupItemRecyclerViewAdapter.ItemCheckedChangeListener itemCheckedChangeListener;
    private GroupItemRecyclerViewAdapter itemRecyclerViewAdapter;
    private App app;

    @Inject
    public GroupsRecyclerViewAdapter(List<Group> groups, boolean hasPlaceHolder, GroupItemRecyclerViewAdapter itemsAdapterInstance, App application) {
        this.app = application;
        this.groups = addPlaceHolderIfTrue(hasPlaceHolder, groups);
        this.itemRecyclerViewAdapter = itemsAdapterInstance;
    }

    public void populate(List<Group> groups) {
        this.groups.clear();
        this.groups.addAll(groups);
        notifyDataSetChanged();
    }

    private List<Group> addPlaceHolderIfTrue(boolean hasPlaceHolder, List<Group> groups) {
        if (hasPlaceHolder && groups.isEmpty()) {
            groups = app.component().getGroupsPlaceHolder();
        }
        return groups;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        MaterialCardView cardView = (MaterialCardView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.groups, viewGroup, false);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        MaterialCardView root = viewHolder.root;
        viewHolder.groupName = root.findViewById(R.id.group_name);
        viewHolder.groupName.setText(groups.get(i).getGroupName());
        viewHolder.recyclerView = root.findViewById(R.id.items_recycler_view);
        viewHolder.recyclerView.setLayoutManager(app.component().getLinearLayoutManager());
        viewHolder.recyclerView.setNestedScrollingEnabled(false);
        viewHolder.recyclerView.getRecycledViewPool().setMaxRecycledViews(0, getItemCount() * 10);
        itemRecyclerViewAdapter.insertFields(groups.get(i).getItems());
        itemRecyclerViewAdapter.setItemCheckedChangeListener(itemCheckedChangeListener);
        viewHolder.recyclerView.setAdapter(itemRecyclerViewAdapter);
    }

    @Override
    public int getItemCount() {
        return groups.size();
    }

    public void setItemCheckedChangeListener(GroupItemRecyclerViewAdapter.ItemCheckedChangeListener itemCheckedChangeListener) {
        this.itemCheckedChangeListener = itemCheckedChangeListener;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        MaterialCardView root;
        RecyclerView recyclerView;
        TextView groupName;

        ViewHolder(@NonNull MaterialCardView itemView) {
            super(itemView);
            root = itemView;
        }
    }
}
